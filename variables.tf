variable "azure_devops_project_name" {
  type = string
  default = ""
}

variable "azure_devops_project_prefix" {
  type = string
  default = "PR.Data"
}

variable "azure_devops_pat" {
  type = string
}

variable "project_name" {
  type = string
}

/*
variable "azure_devops_enabled" {
  type = bool
}
*/

variable "azure_devops_org_service_url" {
  type = string
}

variable "project_application_client_id" {
  type = string
}

variable "environment" {
  type = string
}

variable "azure_devops_groups" {
  description = "A list of Azure DevOps groups (must be present in the Orga)"
  type = list(string)
  default = []
}



