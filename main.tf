locals {
  #azure_devops_project_id = var.azure_devops_enabled ? azuredevops_project.default[0].id : data.azuredevops_project.project[0].id
  azure_devops_project_id = var.azure_devops_project_name != "" ? data.azuredevops_project.existing[0].id : azuredevops_project.default[0].id
  azure_devops_project_name = var.azure_devops_project_name != "" ? var.azure_devops_project_name : "${var.azure_devops_project_prefix}.${replace(var.project_name, " ", "")}"
}

data "azurerm_subscription" "current" {
}

data "azuredevops_project" "existing" {
  count = var.azure_devops_project_name != "" ? 1 : 0
  #project_name = "${var.azure_devops_project_prefix}.${replace(var.azure_devops_project_name, " ", "")}"
  name = var.azure_devops_project_name
}

resource "azuredevops_project" "default" {
  #count = var.azure_devops_enabled ? 1 : 0
  count = var.azure_devops_project_name != "" ? 0 : 1
  name = "${var.azure_devops_project_prefix}.${replace(var.project_name, " ", "")}"
  version_control = "Git"
  work_item_template = "Scrum"
  
  lifecycle {
    ignore_changes = [work_item_template, description]
  }
}

#The key is a fake one to be able to create the resource
resource "azuredevops_serviceendpoint_azurerm" "endpointazure" {
  count = var.project_application_client_id == "" ? 0 : 1
  project_id            = local.azure_devops_project_id
  service_endpoint_name = "Azure ${data.azurerm_subscription.current.display_name} ${var.environment}"
  credentials {
    serviceprincipalid  = var.project_application_client_id
    serviceprincipalkey = "62a40c2a-a441-474f-a606-b733161c1a51"
  }
  azurerm_spn_tenantid      = data.azurerm_subscription.current.tenant_id
  azurerm_subscription_id   = data.azurerm_subscription.current.subscription_id
  azurerm_subscription_name = data.azurerm_subscription.current.display_name
}


/*
// This section assigns users from AAD into a pre-existing group in AzDO
data "azuredevops_group" "group" {
  count = var.environment == "dev" ? 1 : 0
  #count = var.azure_devops_enabled ? 1 : 0
  #project_id = azuredevops_project.default[0].id
  project_id = local.azure_devops_project_id
  name       = "${azuredevops_project.default[0].project_name} Team"
}
*/

/*
data "external" "dev-groups" {
  count = var.azure_devops_enabled ? length(var.azure_devops_groups) : 0
  program = ["az", "devops", "security", "group", "list", "--scope", "organization",  "--query", "graphGroups[?displayName=='${var.azure_devops_groups[count.index]}'] | [0]"]
}

# Use of Azure CLI. Data source for azuredevops_user_entitlement do not exits. 

resource "null_resource" "groups" {
  count = var.azure_devops_enabled ? length(var.azure_devops_groups) : 0
  provisioner "local-exec" {
    command = "az devops security group membership add --organization \"${var.azure_devops_org_service_url}\" --member-id ${data.external.dev-groups[count.index].result["descriptor"]} --group-id '${data.azuredevops_group.group[0].descriptor}'"
    working_dir = path.module
    environment = {
      AZURE_DEVOPS_EXT_PAT = var.azure_devops_pat
    }
  }
}
*/